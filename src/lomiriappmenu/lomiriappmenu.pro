TARGET = lomiriappmenu
TEMPLATE = lib

QT -= gui
QT += core-private theme_support-private dbus

CONFIG += plugin no_keywords

# CONFIG += c++11 # only enables C++0x
QMAKE_CXXFLAGS += -fvisibility=hidden -fvisibility-inlines-hidden -std=c++11 -Werror -Wall
QMAKE_LFLAGS += -std=c++11 -Wl,-no-undefined

CONFIG += link_pkgconfig
PKGCONFIG += gio-2.0

DBUS_INTERFACES += com.lomiri.MenuRegistrar.xml

HEADERS += \
    theme.h \
    gmenumodelexporter.h \
    gmenumodelplatformmenu.h \
    logging.h \
    menuregistrar.h \
    registry.h \
    themeplugin.h \
    lomiriappmenuextraactionhandler.h \
    ../shared/lomiritheme.h

SOURCES += \
    theme.cpp \
    gmenumodelexporter.cpp \
    gmenumodelplatformmenu.cpp \
    menuregistrar.cpp \
    registry.cpp \
    themeplugin.cpp \
    lomiriappmenuextraactionhandler.cpp

OTHER_FILES += \
    lomiriappmenu.json

# Installation path
target.path +=  $$[QT_INSTALL_PLUGINS]/platformthemes

INSTALLS += target
